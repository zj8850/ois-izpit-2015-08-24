var express = require('express');

var app = express();
app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendfile(__dirname + '/public/seznam.html');
});


app.get('/api/seznam', function(req, res) {
	res.send(noviceSpomin);
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Dodajanje novic)
 */
app.get('/api/dodaj', function(req, res) {
  var naslov = req.query.naslov;
  var povzetek = req.query.povzetek;
  var kategorija = req.query.kategorija;
  var postnaStevilka = req.query.postnaStevilka;
  var kraj = req.query.kraj;
  var povezava = req.query.povezava;
  
  if (naslov == "" || povzetek == "" || kategorija == "" || postnaStevilka == "" || kraj == "" || povezava == "") {
    res.send('Napaka pri dodajanju novice!');
  } else {
    var id;
    var len = noviceSpomin.length-1;
    if (len==-1) {
      id = 1;
    } else {
      id = parseInt(noviceSpomin[len]["id"])+1;
    }
    noviceSpomin.push({ 
      id: id,
      naslov: naslov, 
      povzetek: povzetek,
      kategorija: kategorija,
      postnaStevilka: postnaStevilka,
      kraj: kraj,
      povezava: povezava
    });
    res.redirect('/');
	}
  
});



/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Brisanje novice)
 */
app.get('/api/brisi', function(req, res) {
	var id = req.query.id;
	var uspesno = false;
	if (id != "") {
		for (i in noviceSpomin) {
			if (noviceSpomin[i].id == id) {
				noviceSpomin.splice(i, 1);
				uspesno = true;
				break;
			}
		}
		if (uspesno) {
			res.redirect('/');
		} else {
			res.send("Novica z id-jem " + id + " ne obstaja.<br/><a href='javascript:window.history.back()'>Nazaj</a>");
		}
	} else {
		res.send('Napačna zahteva!');
	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var noviceSpomin = [
  {
    id: 1,
    naslov: 'Slovenija in korupcija: končali smo v družbi Mehike in Kolumbije',
    povzetek: 'Slovenija krši mednarodne zaveze v boju proti podkupovanju, opozarjajo pri slovenski podružnici TI. Konvencijo o boju proti podkupovanju tujih javnih uslužbencev v mednarodnem poslovanju OECD namreč izvajamo "malo ali nič".',
    kategorija: 'novice',
    postnaStevilka: 1000,
    kraj: 'Ljubljana',
    povezava: 'http://www.24ur.com/novice/slovenija/slovenija-in-korupcija-koncali-smo-v-druzbi-mehike-in-kolumbije.html'
  }, {
    id: 2,
    naslov: 'V Postojni udaren začetek festivala z ognjenim srcem',
    povzetek: 'V Postojni se je z nastopom glasbenega kolektiva The Stroj začel tradicionalni dvotedenski festival Zmaj ma mlade.',
    kategorija: 'zabava',
    postnaStevilka: 6230,
    kraj: 'Postojna',
    povezava: 'http://www.rtvslo.si/zabava/druzabna-kronika/v-postojni-udaren-zacetek-festivala-z-ognjenim-srcem/372125'
  }
];
